package com.cordova.plugin.fallbackmediasplitter;

import org.apache.cordova.PluginResult;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.SequenceInputStream;

import java.util.ArrayList;
import java.util.List;
import android.util.Log;

// import javax.swing.JOptionPane;

import android.media.AudioManager;
import android.media.MediaPlayer;

import android.os.Build;
import android.os.Environment;
import android.os.Handler;

import android.content.Context;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FallbackMediaSplitter extends CordovaPlugin {

  private MediaPlayer mp = new MediaPlayer();
  public int startTime, endTime, duration;
  String inputName, outputName;
  FileInputStream fis;

  private static final String TAG = "CordovaMediaSplitter";
   
  @Override 
  public boolean execute(String action, JSONArray args, CallbackContext callbackContext) {
    if(action.equals("readMp3")) {
            
            // Parsing JSONObject to Android variables
            try {
                JSONObject options = args.getJSONObject(0);
                inputName = options.getString("inputName");                               
                Log.d(TAG, "CordovaMediaSplitter.java.readMp3("+inputName+")");
            } catch (JSONException e) {
                Log.d(TAG, "JSONException:"+e.getMessage());
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
            }

            if(inputName != null && !inputName.isEmpty()) {

                //Preparing MediaPlayer to get metadata
                FileDescriptor fd = null;            
                try {
                  fis = new FileInputStream(inputName);
                } catch (FileNotFoundException e) {
                  Log.d(TAG, "FileNotFoundException in FIS:"+e.getMessage());
                  callbackContext.error("Error encountered: " + e.getMessage());
                  return false;
                }
                
                

                try {
                  fd = fis.getFD();
                  mp.reset();
                  mp.setDataSource(fd);
                  mp.prepare(); 
                } catch (IOException e) {
                  Log.d(TAG, "IOException in FD & MP:"+e.getMessage());
                  callbackContext.error("Error encountered: " + e.getMessage());
                  return false;
                }
                
                //Getting metadata
                duration = mp.getDuration();       

                Log.d(TAG,"Duration of "+inputName+" is "+duration);
            } else {
                callbackContext.error("Error encountered: Missing input file name");
                return false;
            }
            
            // Send a positive result to the callbackContext
            final PluginResult result = new PluginResult(PluginResult.Status.OK, duration);
            callbackContext.sendPluginResult(result);
    } else if (action.equals("splitMp3")){

            // Parsing JSONObject to Android variables
            try {                
                JSONObject options = args.getJSONObject(0);
                Log.d(TAG, "CordovaMediaSplitter.java.readMp3("+options+")"); 

                inputName = options.getString("inputName");
                outputName = options.getString("outputName");
                startTime = Integer.parseInt(options.getString("startTime"));
                endTime = Integer.parseInt(options.getString("endTime"));
                duration = Integer.parseInt(options.getString("duration"));

                
            } catch (JSONException e) {
                Log.d(TAG, "JSONException:"+e.getMessage());
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
            }

            int x = 0;
            long size;
            double bitrate;

            FileInputStream fis = null;
            FileOutputStream fos = null;

            try {

              fis = new FileInputStream(inputName);

              File f = new File(inputName);
              size = f.length();
              Log.d(TAG,"size" + size);
              bitrate = (double) (size * 8) / (1000 * duration);
              Log.d(TAG,"bitr" + bitrate);
              byte[] buffer = new byte[512];

              fos = new FileOutputStream(outputName);
              long k = 0;
              while ((x = fis.read(buffer)) != -1) {
                  k = k + x;
                  if ((k >= (startTime * bitrate * 1000) / 8) && (k <= (endTime * bitrate * 1000) / 8)) {   
                        Log.d(TAG,"K="+k+"; X="+x);                    
                        fos.write(buffer, 0, x);
                  }
              }
            }// try
            catch (FileNotFoundException e) {
                Log.d(TAG, "FileNotFoundException in Splitting FIS:"+e.getMessage());
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
            } catch (IOException e) {
                Log.d(TAG, "IOException in Splitting FIS:"+e.getMessage());
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
            } finally {

              try {              
                fis.close();
                fos.close();
                Log.d(TAG,"File saved.");
                final PluginResult result = new PluginResult(PluginResult.Status.OK, outputName);
                callbackContext.sendPluginResult(result);
              } catch (Exception e) {
                Log.d(TAG, "Exception in closing fis+fos:"+e.getMessage());
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
              }
            }
    }
    else if(action.equals("echo")) {
            Log.d(TAG, "CordovaMediaSplitter.java.echo()");
            // Parsing JSONObject to Android variables
            try {
                JSONObject options = args.getJSONObject(0);
                String message = options.getString("message");    
                Toast.makeText(cordova.getActivity(),message, Toast.LENGTH_SHORT).show();                        
            } catch (JSONException e) {
                callbackContext.error("Error encountered: " + e.getMessage());
                return false;
            }
            // Send a positive result to the callbackContext
            PluginResult pluginResult = new PluginResult(PluginResult.Status.OK);
            callbackContext.sendPluginResult(pluginResult);   
    } else {
        callbackContext.error("\"" + action + "\" is not a recognized action.");
        return false;
    }
    return true;
  }

  // public void Trim(String input, String output, int startTime, int endTime, int duration){
          
  //     }



  class Mp3filter implements FilenameFilter {

    @Override
    public boolean accept(File arg0, String arg1) {
      // TODO Auto-generated method stub
      return (arg1.endsWith(".mp3"));
    }
  }



  }

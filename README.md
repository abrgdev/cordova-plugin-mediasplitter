Cordova MP3 Splitter Plugin
======

A Cordova plugin to split MP3 audio files.
Using snippets from https://github.com/vatsal13/mp3-Trimmer-and-Joiner by https://github.com/vatsal13

Usage
--------

### Plugin Installation
Do this in terminal/powershell.

```terminal
cd path/to/your/project
cordova plugin add --link /path/to/cordova-plugin-mediasplitter
```

### Plugin Declaration
Put this after importing ionic dependencies in app/page.

```javascript
declare var cordova: any;
```

### Read MP3 Duration
Before trimming the file, the selected audio file must be sent to this function to return its duration.

```javascript
cordova.plugins.CordovaMediaSplitter.readMp3(inputName, onSuccess, onError);
// inputName: native path and filename of mp3 audio input
//			  P.S. inputName only accept native file path without "file://" format.
//			  Example: /storage/emulated/0/Download/Example.mp3
// onSuccess: return duration of mp3 audio input in callback
```

### Trim MP3 according to starting point and ending point in miliseconds.
Do this after getting audio duration.

```javascript
cordova.plugins.CordovaMediaSplitter.splitMp3(inputName, outputName, startTime, endTime, duration, onSuccess, onError);
// inputName: native path and filename of mp3 audio input
//			  P.S. inputName only accept native file path without "file://" format.
//			  Example: /storage/emulated/0/Download/Example.mp3
// outputName: native path to save a filename of splitted mp3 audio
//			  P.S. outputName only accept native file path without "file://" format.
//			  Example: /storage/emulated/0/Download/Splitted.mp3
// startTime: starting point to split in miliseconds
// endTime: ending point to split in miliseconds
// duration: duration of audio file (get from readMp3 function)
```




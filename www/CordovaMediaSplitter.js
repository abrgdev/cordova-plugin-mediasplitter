//Requirements

var exec = cordova.require('cordova/exec');

var PLUGIN_NAME = 'FallbackMediaSplitter';

//Initiate all functions below
var CordovaMediaSplitter = {
  echo: function(message, onSuccess, onError) {
  	console.log("FallbackMediaSplitter.echo");
    var args = {};
    args.message = message;
    exec(onSuccess, onError, PLUGIN_NAME, 'echo', [args]);
  },
  readMp3: function(inputName, onSuccess, onError) {
    console.log("FallbackMediaSplitter.readMp3");
    var args = {};
    args.inputName = inputName;
    exec(onSuccess, onError, PLUGIN_NAME, 'readMp3', [args]);
  },
  splitMp3: function(inputName, outputName, startTime, endTime, duration, onSuccess, onError) {
    console.log("FallbackMediaSplitter.splitMp3");
    var args = {};
    args.inputName = inputName;
    args.outputName = outputName;
    args.startTime = startTime;
    args.endTime = endTime;
    args.duration = duration;
    exec(onSuccess, onError, PLUGIN_NAME, 'splitMp3', [args]);
  }
 //  readFile: function(inputFile, onSuccess, onError) {
 //  	var args = {};
	// args.inputFile = inputFile;
 //  	console.log("CordovaMediaSplitter.writeFile");
 //    exec(onSuccess, onError, PLUGIN_NAME, 'readFile', [args]);
 //  },
 //  writeFile: function(outputFile, startTime, endTime, onSuccess, onError) {
 //  	var args = {};
	// args.outputFile = outputFile;
	// args.startTime = startTime;
	// args.endTime = endTime;
 //  	console.log("CordovaMediaSplitter.readFile");
 //    exec(onSuccess, onError, PLUGIN_NAME, 'writeFile', [args]);
 //  }
};

//Exports module
module.exports = CordovaMediaSplitter;





